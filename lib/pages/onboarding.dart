import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:time_pass/pages/home_page.dart';

class OnboardingPage extends StatelessWidget {
  const OnboardingPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            height: 490,
            width: 375,
            child: Image.asset("assets/images/Subtract.png"),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: Container(
              width: 323,
              height: 118,
              child: Column(
                children: [
                  Text("We are here to make your holiday easier", style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 24,

                  ),textAlign: TextAlign.center,),
               Container(
                       width: 306,

                       child:  Text("Lorem Ipsum is simply dummy text of the printing and typesetting industry.", style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 14,
                         color: Color(0XFF78828A)

                ),textAlign: TextAlign.center,)),


                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 30),
            child: InkWell(
              onTap: (){
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => const HomePage()));
              },
              child: Container(
                height: 58,
                width: 327,
                decoration: BoxDecoration(
                  color: Color(0xff7C73C3),
                  borderRadius: BorderRadius.circular(24),

                ),
                child: Center(
                  child: Text('Get Started', style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w800
                  ), textAlign: TextAlign.center,),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Container(
              height: 24,
              width: 251,
              child: Row(
                children: [
                  Text('Don’t have an account?',style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16, ),textAlign: TextAlign.center,),
                  Text(' Register',style: TextStyle(fontWeight: FontWeight.w600,fontSize: 16,color: Color(0XFF009B8D) ),textAlign: TextAlign.center,),
                ],
              )
            ),
          )
        ],
      ),
    );
  }
}
