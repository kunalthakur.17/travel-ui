import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(left: 18, right: 18),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Image.asset("assets/images/1 70.png", scale: 2.9),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Hi, Andy",
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 18,
                                ),
                              ),
                              Row(
                                children: [
                                  Image.asset(
                                    "assets/images/bxs-map (1) 1.png",
                                    scale: 4,
                                  ),
                                  Text(
                                    "Netherlands",
                                    style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 12,
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 46,
                      width: 46,
                      child: Image.asset("assets/images/Action.png", scale: 2),
                    )
                  ],
                ),
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 24),
                      child: Container(
                        height: 52,
                        width: 327,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(24),
                          color: Color(0XFFF6F8FE),
        
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 5,right: 5),
                          child: Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Icon(Icons.search_outlined),
                              ),
                              Expanded(
                                child: TextField(
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'Search...',
                                  ),
                                ),
                              ),
                              Image.asset("assets/images/Filter.png",scale: 2.9,)
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 24),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          Image.asset("assets/images/flight 1.png",scale: 2.4,),
                          Padding(
                            padding: const EdgeInsets.only(top: 25),
                            child: Text("Airport",style: TextStyle(fontSize: 14,fontWeight: FontWeight.w500,color: Color(0XFF78828A)),),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Image.asset("assets/images/car 1.png",scale: 2.4,),
                          Padding(
                            padding: const EdgeInsets.only(top: 25),
                            child: Text("Taxi",style: TextStyle(fontSize: 14,fontWeight: FontWeight.w500,color: Color(0XFF78828A)),),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Image.asset("assets/images/hotel 1.png",scale: 2.4,),
                          Padding(
                            padding: const EdgeInsets.only(top: 25),
                            child: Text("Hotel",style: TextStyle(fontSize: 14,fontWeight: FontWeight.w500,color: Color(0XFF78828A)),),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Image.asset("assets/images/Category.png",scale: 2.4,),
                          Padding(
                            padding: const EdgeInsets.only(top: 25),
                            child: Text("Category",style: TextStyle(fontSize: 14,fontWeight: FontWeight.w500,color: Color(0XFF78828A)),),
                          ),
                        ],
                      ),
        
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 24),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Frequently visited",style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 18
                      ),),
                      Image.asset("assets/images/Slider.png",scale: 3.8,)
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 232,
                      width: 156,
                      child: Column(
                        children: [
                          Container(
                            height: 156,
                            width: 150,
                            child: Image.asset("assets/images/Group 1000003468.png"),
                          ),
                          Row(
                            children: [
                              Text("Tahiti Beach",style: TextStyle(fontSize: 14,fontWeight: FontWeight.w600),),
                            ],
                          ),
                          Row(
                            children: [
                              Image.asset("assets/images/bxs-map (1) 1.png",scale: 3,),
                              Text("Polynesia, French ",style: TextStyle(fontSize: 10,fontWeight: FontWeight.w500),),
                              ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Text("\$235"),
                              ),
                              Container(
                                child: Row(
                                  children: [
                                    Icon(Icons.star,color: Color(0XFFFFCD1A),),
                                    Text("4.4",style: TextStyle(
                                      color: Color(0XFFFFCD1A),
                                      fontWeight: FontWeight.w600,
                                      fontSize: 15,
                                    ),),
                                    Text("(32)",style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 15,
                              ),),
                                  ],
                                ),
                              )
        
                            ],
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: 232,
                      width: 156,
                      child: Column(
                        children: [
                          Container(
                            height: 156,
                            width: 150,
                            child: Image.asset("assets/images/Group 1000003469.png"),
                          ),
                          Row(
                            children: [
                              Text("St. Lucia Mountain",style: TextStyle(fontSize: 14,fontWeight: FontWeight.w600),),
                            ],
                          ),
                          Row(
                            children: [
                              Image.asset("assets/images/bxs-map (1) 1.png",scale: 3,),
                              Text("South America",style: TextStyle(fontSize: 10,fontWeight: FontWeight.w500),),
                              ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Text("\$132"),
                              ),
                              Container(
                                child: Row(
                                  children: [
                                    Icon(Icons.star,color: Color(0XFFFFCD1A),),
                                    Text("4.4",style: TextStyle(
                                      color: Color(0XFFFFCD1A),
                                      fontWeight: FontWeight.w600,
                                      fontSize: 15,
                                    ),),
                                    Text("(41)",style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 15,
                              ),),
                                  ],
                                ),
                              )
        
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 24),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("On Budget Tour",style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 18
                      ),),
                      Text("See All",style: TextStyle(fontWeight: FontWeight.w500, fontSize: 14,color: Color(0XFF009B8D)),)
                    ],
                  ),
                ),
                
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 86,
                      width: 86,
                      child: Image.asset("assets/images/on budget tour 1.png"),
                    ),
                    Column(
                      children: [
                        Text("Ledadu Beach",style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 18
                        ),),
                        Text("3 days 2 nights",),
                        Padding(
                          padding: const EdgeInsets.only(top: 12),
                          child: Row(
                            children: [
                              Image.asset("assets/images/bxs-map (1) 1.png",scale: 3,),
                              Text("Australia")
                            ],
                          ),
                        )
                      ],
                    ),
                    Row(
                      children: [
                        Text("\$ 20",style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w700
                        ),),
                        Text("/Person")
                      ],
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 86,
                        width: 86,
                        child: Image.asset("assets/images/on budget tour 1.png"),
                      ),
                      Column(
                        children: [
                          Text("Endigada Beach",style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 18
                          ),),
                          Text("5 days 4 nights",),
                          Padding(
                            padding: const EdgeInsets.only(top: 12),
                            child: Row(
                              children: [
                                Image.asset("assets/images/bxs-map (1) 1.png",scale: 3,),
                                Text("Australia")
                              ],
                            ),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Text("\$ 25",style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w700
                          ),),
                          Text("/Person")
                        ],
                      )
                    ],
                  ),
                ),
        
              ],
            ),
          ),
        ),
      ),
    );
  }
}
